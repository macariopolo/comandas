package edu.uclm.esi.disoft.comandas.dominio;

import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONObject;

public class Mesa {
	private int id;
	private Comanda comandaActual;

	public Mesa(int id) {
		this.id=id;
	}

	public static ConcurrentHashMap<Integer, Mesa> loadMesas() {
		ConcurrentHashMap<Integer, Mesa> mesas=new ConcurrentHashMap<>();
		for (int i=1; i<=10; i++)
			mesas.put(i, new Mesa(i));
		return mesas;
	}

	public JSONObject toJSONObject() {
		JSONObject jso=new JSONObject();
		jso.put("id", this.id);
		jso.put("estado", comandaActual==null ? "Libre" : "Ocupada");
		return jso;
	}
	
	public boolean estaLibre() {
		return comandaActual==null;
	}

	public void abrir() throws Exception {
		if (comandaActual!=null)
			throw new Exception("La mesa ya está abierta. Elige otra");
		comandaActual=new Comanda();
	}

	public void cerrar() throws Exception {
		if (comandaActual==null)
			throw new Exception("La mesa ya está cerrada");
		comandaActual.cerrar();
		comandaActual=null;
	}

	public void addToComanda(Plato plato, int unidades) {
		this.comandaActual.add(plato, unidades);
	}

	public JSONObject estado() {
		JSONObject jso=new JSONObject();
		jso.put("id", this.id);
		if (comandaActual==null) 
			jso.put("estado", "Libre");
		else {
			jso.put("comanda", this.comandaActual.toJSONObject());
		}
		return jso;
	}
}
