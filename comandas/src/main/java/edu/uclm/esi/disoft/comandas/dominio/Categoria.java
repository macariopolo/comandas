package edu.uclm.esi.disoft.comandas.dominio;

import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class Categoria {
	private int id;
	private String nombre;
	private JSONArray jsaPlatos;
	private ConcurrentHashMap<Integer, Plato> platos;
	
	public Categoria(int id, String nombre) {
		this.id=id;
		this.nombre=nombre;
		this.jsaPlatos=new JSONArray();
		this.platos=new ConcurrentHashMap<>();
	}

	public void add(Plato... platos) {
		for (int i=0; i<platos.length; i++) {
			this.jsaPlatos.put(platos[i].toJSONObject());
			this.platos.put(platos[i].getId(), platos[i]);
		}
	}

	public Plato find(int idPlato) {
		return this.platos.get(idPlato);
	}
	
	public JSONArray getPlatos() {
		return jsaPlatos;
	}

	public JSONObject toJSONObject() {
		JSONObject jso=new JSONObject();
		jso.put("id", this.id);
		jso.put("nombre", this.nombre);
		jso.put("platos", this.jsaPlatos);
		return jso;
	}
}
