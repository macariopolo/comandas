package edu.uclm.esi.disoft.comandas.dominio;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import org.json.JSONArray;
import org.json.JSONObject;

public class Manager {
	private ConcurrentHashMap<Integer, Mesa> mesas;
	private JSONArray jsaCategorias;
	private ConcurrentHashMap<Integer, Categoria> categorias;
	
	private Manager() {
		mesas=Mesa.loadMesas();
		crearCategorias(); // Método sólo para ir probando.
	}
	
	private void crearCategorias() {
		Categoria arroces=new Categoria(1, "Arroces");
		arroces.add(new Plato(1, "Arroz blanco", 12.0), new Plato(2, "Arroz con bogavante", 18.0), new Plato(3, "Paella", 16.0));
		Categoria ensaladas=new Categoria(2, "Ensaladas");
		ensaladas.add(new Plato(4, "Mixta", 8.0), new Plato(5, "César", 8.50), new Plato(6, "Atún", 9.50), new Plato(7, "Californiana", 7.0));
		this.jsaCategorias=new JSONArray();
		this.jsaCategorias.put(arroces.toJSONObject());
		this.jsaCategorias.put(ensaladas.toJSONObject());
		
		this.categorias=new ConcurrentHashMap<>();
		this.categorias.put(1, arroces);
		this.categorias.put(2, ensaladas);
	}

	private static class ManagerHolder {
		static Manager singleton=new Manager();
	}
	
	public static Manager get() {
		return ManagerHolder.singleton;
	}
	
	public void abrirMesa(int id) throws Exception {
		Mesa mesa=mesas.get(id);
		mesa.abrir();
	}
	
	public void cerrarMesa(int id) throws Exception {
		Mesa mesa=mesas.get(id);
		mesa.cerrar();
	}
	
	public void addToComanda(int idMesa, JSONArray platos) throws Exception {
		Mesa mesa=mesas.get(idMesa);
		if (mesa.estaLibre())
			throw new Exception("La mesa " + idMesa + " está libre. Ábrela primero");
		for (int i=0; i<platos.length(); i++) {
			JSONObject jsoPlato=platos.getJSONObject(i);
			int idCategoria=jsoPlato.getInt("idCategoria");
			int idPlato=jsoPlato.getInt("idPlato");
			int unidades=jsoPlato.getInt("unidades");
			Categoria categoria=this.categorias.get(idCategoria);
			Plato plato=categoria.find(idPlato);
			mesa.addToComanda(plato, unidades);
		}
	}
	
	public JSONArray getMesas() {
		JSONArray result=new JSONArray();
		Enumeration<Mesa> eMesas = this.mesas.elements();
		Mesa mesa;
		while (eMesas.hasMoreElements()) {
			mesa=eMesas.nextElement();
			result.put(mesa.toJSONObject());
		}
		return result;
	}
	
	public JSONObject getEstadoMesa(int id) {
		return this.mesas.get(id).estado();
	}
	
	public JSONArray getCategorias() {
		return this.jsaCategorias;
	}
}
