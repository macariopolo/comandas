package edu.uclm.esi.disoft.comandas.dominio;

import org.json.JSONObject;

public class Plato {
	private int id;
	private String nombre;
	private double precio;

	public Plato(int id, String nombre, double precio) {
		this.id=id;
		this.nombre=nombre;
		this.precio=precio;
	}

	public JSONObject toJSONObject() {
		JSONObject jso=new JSONObject();
		jso.put("id", id);
		jso.put("nombre", nombre);
		jso.put("precio", precio);
		return jso;
	}

	public Integer getId() {
		return this.id;
	}
}
